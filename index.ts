import express from 'express';
import * as productos from './productos';


const app = express();
app.use(express.json());

app.get('/', function (request, response){
    response.send('¡Bienvenido a Express!');
})

app.get('/productos', function (request, response){
    response.send(productos.getStock())
})

app.post('/productos', function(request, response) {
    const body = request.body
    const id = productos.getStock().length
    productos.storeProductos(body, id)
    response.send('Se ha agregado un producto nuevo')
})


app.delete('/productos/:id', function(request, response) {
    const id = Number (request.params.id)
    productos.deleteProductos(id);
    response.send('El producto ha sido eliminado')
}) 


app.listen(3000, function(){

    console.info('Servidor escuchando en http://localhost:3000');
})




